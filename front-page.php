<?php get_header();
/**
*
* @package WordPress
* @subpackage Grayscale
*/
?>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" onclick="gtag('event', 'click', {'event_category': 'menu','event_label': 'logo'});" href="#page-top">
        <img src="<?php echo get_template_directory_uri(); ?>/img/logo_hires.png" alt="A.S.Z.V. SPONS" height="88px" />
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about" onclick="gtag('event', 'click', {'event_category': 'menu','event_label': 'about'});">
              Over ons
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#projects" onclick="gtag('event', 'click', {'event_category': 'menu','event_label': 'trainingstijden'});">
              Trainingstijden
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#zwemcursus" onclick="gtag('event', 'click', {'event_category': 'menu','event_label': 'zwemcursus'});">
              Zwemcursus
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#proefzwemmen" onclick="gtag('event', 'click', {'event_category': 'menu','event_label': 'lid worden'});">
              Lid worden
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/old" onclick="gtag('event', 'click', {'event_category': 'menu','event_label': 'leden'});">
              Voor leden
            </a>
          </li>
        </ul>
      </div>
      <ul><?php pll_the_languages();?></ul>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <!--<h1 class="mx-auto my-0 text-uppercase">A.S.Z.V. SPONS</h1>-->
        <div style="padding:56.25% 0 0 0;position:relative;">
          <iframe src="https://player.vimeo.com/video/231041027?color=fbed21&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;"
          frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <script src="https://player.vimeo.com/api/player.js"></script>
        <h2 class="text-kanariegeel-100 mx-auto">Dé studentenzwemvereniging van Amsterdam, voor sport &amp; gezelligheid.</h2>
        <a href="#proefzwemmen" class="btn btn-primary js-scroll-trigger">Kom proeftrainen!</a>
        <a href="#signup" class="btn btn-primary btn-primary-CTA js-scroll-trigger">Word lid!</a>
      </div>
    </div>
  </header>

  <!-- About Section -->
  <section id="about" class="about-section text-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto text-white-50">
          <h2 class="text-white mb-4"><?php echo get_post(2)->post_title ?></h2>
            <?php
              echo get_post(2, "OBJECT", 'display')->post_content;
            ?>
          </div>
        </div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/bestuur.jpg" class="img-fluid" alt="">
      </div>
    </section>

    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light">
      <div class="container">
        <!-- Project One Row -->
        <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
          <div class="col-lg-12">
            <div class="bg-black text-center h-100 project">
              <div class="d-flex h-100">
                <div class="project-text w-100 my-auto text-center text-lg-left text-white">
                  <h4 class="text-white"><?php echo get_post(12)->post_title ?></h4>
                  <?php
                    echo get_post(12, "OBJECT", 'display')->post_content;
                  ?>
                  <hr class="d-none d-lg-block mb-0 ml-0">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

        <section id="zwemcursus" class="projects-section bg-light">
          <div class="container">
            <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
              <div class="col-lg-12">
                <div class="bg-black text-center h-100 project">
                  <div class="d-flex h-100">
                    <div class="project-text w-100 my-auto text-center text-lg-left text-white-50">
                      <h4 class="text-white"><?php echo get_post(14)->post_title ?></h4>
                      <?php
                        echo get_post(14, "OBJECT", 'display')->post_content;
                      ?>
                      <hr class="d-none d-lg-block mb-0 ml-0">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- Signup Section -->
        <section id="proefzwemmen" class="signup-section">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-lg-8 mx-auto text-center">
                <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 1, 'title' => true, 'description' => false ) ); ?>
                <!--<p class="text-kanariegeel-100">of </p>
                <p class="text-kanariegeel-100">Ga na het <a href="https://aszvspons.nl/aanmelden/SPONS.php">aanmeldformulier</a>, en meld je direct aan!</p>-->
              </div>
            </div>
            <div id="signup" class="row" style="padding-top:80px">
              <div class="col-md-10 col-lg-8 mx-auto text-center">
                <h2 class="text-white mb-5">Of word je direct lid?</h2>
                <p class="text-white">Ga na het <a href="https://aszvspons.nl/aanmelden/SPONS.php">aanmeldformulier</a>, en meld je direct aan!</p>
              </div>
            </div>
          </div>
        </section>
        <!-- Signup Section
        <section id="signup" class="signup-section">
        <div class="container">
        <div class="row">
        <div class="col-md-10 col-lg-8 mx-auto text-center">

        <i class="far fa-paper-plane fa-2x mb-2 text-white"></i>
        <h2 class="text-white mb-5">Direct lid worden?</h2>
        <p>Ga na het <a href="https://aszvspons.nl/aanmelden/SPONS.php">aanmeldformulier</a>, en meld je direct aan!</p>
      </div>
    </div>
  </div>
</section>
-->

<!-- Contact Section -->
<section class="contact-section bg-black">
  <div class="container">
    <div class="social d-flex justify-content-center">
      <a href="https://www.instagram.com/aszvspons/" class="mx-2">
        <i class="fab fa-instagram"></i>
      </a>
      <a href="https://www.facebook.com/aszvspons" class="mx-2">
        <i class="fab fa-facebook-f"></i>
      </a>
      <a href="https://www.linkedin.com/company/2416859/" class="mx-2">
        <i class="fab fa-linkedin"></i>
      </a>
    </div>
  </div>
</section>

<?php get_footer()?>
