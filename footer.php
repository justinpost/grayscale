<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Grayscale
 */
?>

<!-- Footer -->
<footer class="bg-black small text-center text-white-50">
  <div class="container">
    Copyright &copy; A.S.Z.V. SPONS 2019<br />
    <a href="files/privacypolicy-jan18.pdf" target="_blank" onclick="gtag('event', 'click', {'event_category': 'footer','event_label': 'privacy'});"><?php echo $lang['privacy_policy']; ?></a>
    |
    <a href="files/AV-mar15.pdf" target="_blank" onclick="gtag('event', 'click', {'event_category': 'footer','event_label': 'algemene voorwaarden'});"><?php echo $lang['ToC']; ?></a>
  </div>
</footer>

</body>

</html>
